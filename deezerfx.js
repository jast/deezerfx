let port;
function doConnect() {
	port = browser.runtime.connect();
	port.onDisconnect.addListener((p) => {
		if (p.error) console.log(`Disconnected due to an error: ${p.error.message}`);
		setTimeout(doConnect, 5000);
	});
}
doConnect();

let btnsPlayer = '#page_player';
let btnsTrack = '.player-track .track-actions';
function maybeBtn(base, icon, extra, up) {
	let sel = `${base} svg.svg-icon-${icon}`;
	if (extra) sel += `${extra}`;
	let btn = document.querySelector(sel);
	if (up === undefined) up = 1;
	if (btn) {
		while (up--) btn = btn.parentNode;
		btn.click();
	}
}

function sendMsg(msg) {
	if (port) port.postMessage(msg);
}

port.onMessage.addListener(msg => {
	let player = window.wrappedJSObject.dzPlayer;
	if (!player) {
		console.log("Can't process control message, player not found");
		return;
	}
	switch (msg.type) {
		case 'toggle':
			player.control.togglePause();
			break;
		case 'next':
			player.control.nextSong();
			break;
		case 'prev':
			player.control.prevSong();
		case 'like':
			maybeBtn(btnsTrack, 'love-outline', ':not(.is-active)');
			break;
		case 'dislike':
			maybeBtn(btnsTrack, 'love-outline', '.is-active');
			break;
		case 'block':
			maybeBtn(btnsTrack, 'angry-face');
			setTimeout(() => { maybeBtn(btnsTrack, 'note-block', null, 2) });
			break;
		case 'block_artist':
			maybeBtn(btnsTrack, 'angry-face');
			setTimeout(() => { maybeBtn(btnsTrack, 'profile-block', null, 2) });
		case 'toggle_mute':
			maybeBtn('.player-options', 'volume');
		case 'volume':
			player.control.setVolume(msg.volume);
		default:
			console.log(`Unknown message type '${msg.type}'`);
	}
});

function handler() {
	let player = window.wrappedJSObject.dzPlayer;
	if (!player) {
		sendMsg({'playing': false});
		return;
	}
	let song = player.getCurrentSong();
	if (!song) {
		sendMsg({'playing': false});
		return;
	}
	sendMsg({
		'playing': player.playing,
		'title': song.SNG_TITLE,
		'artist': song.ART_NAME,
		'cover': song.ALB_PICTURE,
		'position': player.getPosition(),
		'duration': parseInt(song.DURATION),
	});
};
setInterval(handler, 1000);
