# DeezerFx

This is a small bundle of a Firefox extension, an AutoHotkey script and a web
application that work together to allow you to control the Deezer web player
with keyboard shortcuts and also access data about the currently playing song.

## Firefox extension

I don't bother maintaining an online version for something as underdocumented as
this, so it's not available via the Addons site, but you can download it (see
top description on GitLab project page) and manually install it from the file.

## AutoHotkey script

This uses standard multimedia keys to provide a fair number of ways to interact
with Deezer. Since we capture these key presses and don't want to lose the
normal media control functions when not using Deezer, all of the shortcuts are
*combinations*:

* Ctrl + prev/play+pause/next triggers the corresponding action in Deezer.
* Holding down next and pressing "b" blocks a song from Flow and skips to the
  next one.
* Holding down next and pressing "d" removes your "like" from the song.
* Holding down next and pressing "l" adds a "like" (favourite) to the song.
* Holding down mute and one of the digits from 1-0 sets Deezer's playback volume
  to 10-100% (digit 0 = 100%).

To make these special combinations of keys possible, the script changes the
behaviour of "mute" and "next" slightly: their normal function is performed only
at the moment you release the key, and only if no other key was pressed in the
meantime. This should be mostly unnoticeable in most cases.

## Web application

This is designed to be run locally on your computer and is **required** for all
of the functionality. It needs Python 3 and the Flask web framework.

I included a `run.sh` shell script to execute it in a UNIX-like shell
environment (e.g. Cygwin or the Linux subsystem in Windows 10). It assumes that
you have set up a virtualenv inside the DeezerFx directory.

### Querying playback info

You get can JSON data about the player state and current song by querying
http://localhost:45526/status. To get a compact HTML view of this data that
auto-refreshes and automatically turns empty while the player is paused or
closed, you can use http://localhost:45526/statuspage. I use this to embed
playback info in various places, e.g. live streams.