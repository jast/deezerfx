#!/usr/bin/env python3

import aiohttp
from aiohttp import web
import asyncio
import cgi
import datetime
import json

routes = web.RouteTableDef()
active_sockets = set()
status_data = {
    'playing': False,
}

def nicetime(secs, ref=None):
    if ref is None:
        ref = secs
    secs = datetime.datetime.utcfromtimestamp(secs)
    res = secs.strftime("%M:%S")
    if ref >= 3600:
        res = '%d:%s' % (secs / 3600, res)
    return res

@routes.get('/statuspage')
async def statuspage(request):
    res = web.Response(content_type='text/html')
    res.text = """
    <body>
        <div id="status" style="display: none;">
            <img id="cover" alt="" width="48" height="48">
            &#x25b6; <strong id="artist"></strong>
            &#xb7; <strong id="title"></strong>
            (<span id="position"></span> / <span id="duration"></span>)
        </div>
        <script>
            setInterval(() => {
                let xhr = new XMLHttpRequest();
                xhr.addEventListener("load", () => {
                    let data;
                    try {
                        data = JSON.parse(xhr.responseText);
                    } catch (ex) {
                        return;
                    }
                    if (!data.playing) {
                        document.querySelector('#status').style.display = 'none';
                        return;
                    }
                    let imgurl = `https://e-cdns-images.dzcdn.net/images/cover/${data.cover}/48x48-000000-80-0-0.jpg`;
                    let cover = document.querySelector('#cover');
                    if (imgurl !== cover.getAttribute('src')) cover.setAttribute('src', imgurl);
                    document.querySelector('#artist').innerText = data.artist;
                    document.querySelector('#title').innerText = data.title;
                    document.querySelector('#position').innerText = data.position;
                    document.querySelector('#duration').innerText = data.duration;
                    document.querySelector('#status').style.display = 'block';
                });
                xhr.addEventListener("error", () => { document.querySelector('#status').style.display = 'none'; });
                xhr.open("GET", "/status");
                xhr.send();
            }, 1000);
        </script>
    </body>
    """
    return res

@routes.get('/status')
async def status(request):
    data = dict(status_data)
    if 'duration' in data:
        data['duration'] = nicetime(status_data['duration'])
        data['position'] = nicetime(status_data['position'], status_data['duration'])
    return web.json_response(data)

@routes.post('/control')
async def control(request):
    if not request.can_read_body:
        raise web.HTTPBadRequest()
    try:
        data = await request.json()
    except:
        print("Invalid control request")
        raise web.HTTPBadRequest()

    for socket in active_sockets:
        await socket.send_json(data)

    return web.json_response({"status": "ok"})

@routes.get('/ws')
async def ws(request):
    socket = web.WebSocketResponse()
    await socket.prepare(request)
    active_sockets.add(socket)

    async for msg in socket:
        if msg.type == aiohttp.WSMsgType.TEXT:
            try:
                data = json.loads(msg.data)
            except:
                print("Invalid WS message received")
                continue
            if isinstance(data, dict) and 'playing' in data:
                global status_data
                status_data = data
            else:
                print("Unexpected data in WS message")
                continue
        elif msg.type == aiohttp.WSMsgType.ERROR:
            print("WS connection closed with exception %s" % socket.exception())

    active_sockets.remove(socket)
    print("WS connection closed")
    return socket

async def startup():
    app = web.Application()
    app.add_routes(routes)
    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, 'localhost', 45526)
    await site.start()
    while True:
        await asyncio.sleep(3600)
    await runner.cleanup()

loop = asyncio.get_event_loop()
loop.run_until_complete(startup())
loop.close()
