let wsConnect = false;
let wsLastConnect = 0;
let ws;

let port;

function portConnected(p) {
	if (port) return;
	port = p;

	port.onMessage.addListener(function(msg) {
		sendWs(msg);
	});
	port.onDisconnect.addListener(function() { port = null; });
}
browser.runtime.onConnect.addListener(portConnected);

function sendWs(data) {
	if (ws && ws.readyState === 1) ws.send(JSON.stringify(data));
}

function connectWs() {
	// Try reconnect every 10 seconds
	let now = +(new Date());
	if (now - wsLastConnect < 10000) return;
	wsLastConnect = now;

	if (ws) ws.close();
	ws = new window.WebSocket("ws://127.0.0.1:45526/ws");
	ws.addEventListener('open', e => { wsConnect = true; });
	ws.addEventListener('error', e => { ws.close(); });
	ws.addEventListener('close', e => { wsConnect = false; });
	ws.addEventListener('message', e => {
		let msg;
		try {
			msg = JSON.parse(e.data);
		} catch (ex) {
			console.log(`Received data in unexpected format: ${ex}`);
			return;
		}
		if (port) port.postMessage(msg);
	});
}

function handler() {
	if (!wsConnect) connectWs();
	if (!port) {
		sendWs({'playing': false});
		return;
	}
};
setInterval(handler, 1000);
