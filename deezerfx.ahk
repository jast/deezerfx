sendControl(data) {
  url := "http://localhost:45526/control"
  http := ComObjCreate("WinHttp.WinHttpRequest.5.1")
  http.Open("POST", url, false)
  http.Send(data)
}

sendSimple(type) {
  sendControl("{""type"":""" . type . """}")
}

sendVolume(value) {
  sendControl("{""type"":""volume"", ""volume"": " . value . "}")
}

Volume_Mute & 1::sendVolume(0.04)
Volume_Mute & 2::sendVolume(0.06)
Volume_Mute & 3::sendVolume(0.09)
Volume_Mute & 4::sendVolume(0.13)
Volume_Mute & 5::sendVolume(0.18)
Volume_Mute & 6::sendVolume(0.25)
Volume_Mute & 7::sendVolume(0.35)
Volume_Mute & 8::sendVolume(0.5)
Volume_Mute & 9::sendVolume(0.71)
Volume_Mute & 0::sendVolume(1)
Volume_Mute::Send {Volume_Mute}
^Volume_Mute::sendVolume(0)
^Media_Next::sendSimple("next")
Media_Next & l::sendSimple("like")
Media_Next & d::sendSimple("dislike")
Media_Next & b::sendSimple("block")
Media_Next & a::sendSimple("block_artist")
Media_Next::Send {Media_Next}
^Media_Prev::sendSimple("prev")
^Media_Play_Pause::sendSimple("toggle")
